#include <vector>
#include <algorithm>
/*
time: O(nlogn)
extra space: O(answer)
*/
namespace std_nlogn {
    int solve(std::vector<int> a, std::vector<int> b) {
        std::sort(a.begin(), a.end());
        std::sort(b.begin(), b.end());

        std::vector<int> intersection;

        std::set_intersection(a.begin(), a.end(),
            b.begin(), b.end(),
            std::back_inserter(intersection));

        return static_cast<int> (intersection.size());
    }
}
