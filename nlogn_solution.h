#include <vector>
#include <algorithm>
/*
time: O(nlogn)
extra space: O(1)
*/
namespace nlogn {
    int solve(std::vector<int> a, std::vector<int> b) {
        std::sort(a.begin(), a.end());
        std::sort(b.begin(), b.end());
        std::size_t i = 0, j = 0;
        int ans = 0;
        while (i < a.size() && j < b.size()) {
            if (a[i] < b[j]) {
                ++i;
            }
            else if (a[i] > b[j]) {
                ++j;
            }
            else {
                ++ans;
                ++i; 
                ++j;
            }
        }
        return ans;
    }
}
