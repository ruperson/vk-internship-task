#include <vector>
#include <algorithm>

#include <ext/pb_ds/assoc_container.hpp>

/*
time: O(n)
extra space O(n)
*/
namespace fast_solution {
    int solve(std::vector<int> a, std::vector<int> b) {
        if (a.size() > b.size()) {
            std::swap(a, b);
        }
        __gnu_pbds::gp_hash_table<int, int> table;
        for (auto x : a) {
            table[x] = 1;
        }
        int result = 0;
        for (auto x : b) {
            result += (table.find(x) != table.end());
        }
        return result;
    }
}