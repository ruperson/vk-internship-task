#include <random>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <unordered_set>
#include "solution.h"
#include "fast_solution.h"
#include "nlogn_solution.h"
#include "std_nlogn_solution.h"

const int TEST_COUNT = 1;

int main() {
    std::cout << "Running . . ." << std::endl;
    std::random_device rd;
    std::mt19937 g(rd());
    std::uniform_int_distribution<int> size_dist(1'000'000, 10'000'000);
    std::uniform_int_distribution<int> dist(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());

    std::clock_t sorting_time = 0, std_time = 0, us_time = 0, fs_time = 0;
    for (int i = 1; i <= TEST_COUNT; ++i) {
        int times_a = size_dist(g);
        int times_b = size_dist(g);
       
        std::unordered_set<int> a;
        for (int j = 0; j < times_a; ++j) {
            a.insert(dist(g));
        }
        std::unordered_set<int> b;
        for (int j = 0; j < times_b; ++j) {
            b.insert(g());
        }        

        std::vector<int> first(a.begin(), a.end()), second(b.begin(), b.end());

        std::shuffle(first.begin(), first.end(), g);
        std::shuffle(second.begin(), second.end(), g);

        std::clock_t before;

        before = clock();
        int f1 = solution::solve(first, second);
        us_time += clock() - before;

        before = clock();
        int f2 = nlogn::solve(first, second);
        sorting_time += clock() - before;

        before = clock();
        int f3 = std_nlogn::solve(first, second);
        std_time += clock() - before;

        before = clock();
        int f4 =  fast_solution::solve(first, second);
        fs_time += clock() - before;

        if (f1 != f2 || f2 != f3 || f3 != f4) {
            std::cout<< "Wrong answer !" << std::endl;
            return 0;
        }
    }

    std::cout << std::setprecision(3);
    std::cout << "Sorting solution"
        << std::endl
        << "time taken: "
        << double(sorting_time) / CLOCKS_PER_SEC << "s."
        << std::endl;
    std::cout << "Unordered set solution"
        << std::endl
        << "time taken: "
        << double(us_time) / CLOCKS_PER_SEC << "s."
        << std::endl;
    std::cout << "Sorting std c++17 solution"
        << std::endl
        << "time taken: "
        << double(std_time) / CLOCKS_PER_SEC << "s."
        << std::endl;
    std::cout << "Fast hashtable solution"
        << std::endl
        << "time taken: "
        << double(fs_time) / CLOCKS_PER_SEC << "s."
        << std::endl;

    
}