#include <vector>
#include <unordered_set>
#include <algorithm>
/*
time: O(n)
extra space O(n)
*/
namespace solution {
    int solve(std::vector<int> a, std::vector<int> b) {
        if (a.size() > b.size()) {
            std::swap(a, b);
        }
        std::unordered_set<int> table(a.begin(), a.end());
        int result = 0;
        for (auto x : b) {
            result += table.count(x);
        }
        return result;
    }
}