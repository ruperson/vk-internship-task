#include <algorithm>
#include <vector>
#include <unordered_set>
#include <limits>
#include <random>
#include <gtest/gtest.h>
#include "solution.h"
#include "fast_solution.h"
#include "nlogn_solution.h"
#include "std_nlogn_solution.h"


namespace {
    /*Parameters which can be changed*/
    const int SHUFFLE_COUNT = 10;
    const int TEST_COUNT = 30;

    void is_equal(int a, int b, int c, int d) {
        EXPECT_EQ(a, b);
        EXPECT_EQ(b, c);
        EXPECT_EQ(c, d);
    }

    std::random_device rd;
    std::mt19937 generator(rd());


    void full_test(std::vector <int> a, std::vector <int> b) {
        for (int i = 1; i <= SHUFFLE_COUNT; ++i) {
            is_equal(fast_solution::solve(a, b), solution::solve(a, b), nlogn::solve(a, b), std_nlogn::solve(a, b));
            is_equal(fast_solution::solve(b, a), solution::solve(b, a), nlogn::solve(b, a), std_nlogn::solve(b, a));
        
            std::shuffle(a.begin(), a.end(), generator);
            std::shuffle(b.begin(), b.end(), generator);

        }
    }
    void quick_test(std::vector <int> a, std::vector <int> b) {
        is_equal(fast_solution::solve(a, b), solution::solve(a, b), nlogn::solve(a, b), std_nlogn::solve(a, b));
    }
}

TEST(correctness, one) {
    std::vector<int> a = { 1 };
    std::vector<int> b = { 1 };
    full_test(a, b);

    std::vector<int> c = { 4 };
    std::vector<int> d = { 2 };
    full_test(c, d);
}

TEST(correctness, two) {
    std::vector<int> a = { 1, 2 };
    std::vector<int> b = { 1, 2 };
    full_test(a, b);

    std::vector<int> c = { 1, 2 };
    std::vector<int> d = { 3, 4};
    full_test(c, d);

    std::vector<int> e = { 1, 2 };
    std::vector<int> f = { 2, 3 };
    full_test(e, f);
}

TEST(correctness, few) {
    std::vector<int> a = { 7 };
    std::vector<int> b = { 1, 2,3,4,5 };
    full_test(a, b);

    std::vector<int> c = { 1, 2, 3, 4, 5 };
    std::vector<int> d = { 3, 4, 5, 6, 7, 8, 9 };
    full_test(c, d);

    std::vector<int> e = { 1, 2, 3, 4 };
    std::vector<int> f = { 1, 2, 3, 5 };
    full_test(e, f);
}

TEST(correctness, little_crazy) {
    auto x = std::numeric_limits<int>::max();
    auto y = std::numeric_limits<int>::min();
    std::vector<int> a = { x };
    std::vector<int> b = { y };
    full_test(a, b);

    std::vector<int> c = { x, y};
    std::vector<int> d = { x, y};
    full_test(c, d);

    std::vector<int> e = { x, y };
    std::vector<int> f = { y };
    full_test(e, f);
}

TEST(correctness, empty) {
    std::vector<int> a;
    full_test(a, a);
    std::vector<int> b = { 1, 2, 3 };
    full_test(a, b);
    std::vector <int> c(12345);
    std::generate(c.begin(), c.end(), generator);
    std::sort(c.begin(), c.end());
    c.erase(std::unique(c.begin(), c.end()), c.end());
    full_test(a, c);
}

TEST(correctness, random) {
   
    std::uniform_int_distribution<int> times_distribution(0, 100'000);
    std::uniform_int_distribution<int> element_distribution(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
    
    for (int i = 1; i <= TEST_COUNT; ++i) {
        int times_a = times_distribution(generator);
        int times_b = times_distribution(generator);
       
        std::unordered_set<int> a;
        for (int j = 0; j < times_a; ++j) {
            a.insert(element_distribution(generator));
        }
        std::unordered_set<int> b;
        for (int j = 0; j < times_b; ++j) {
            b.insert(element_distribution(generator));
        }        

        std::vector<int> first(a.begin(), a.end()), second(b.begin(), b.end());

        std::shuffle(first.begin(), first.end(), generator);
        std::shuffle(second.begin(), second.end(), generator);
        quick_test(first, second);
    }

}

